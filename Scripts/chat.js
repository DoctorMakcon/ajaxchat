﻿$(document).ready(function () {
    $("#btnlogin").click(function () {
        var nickname = $("#textusername").val();
        if (nickname) {
            var href = "/Home?user=" + encodeURIComponent(nickname);
            href = href + "&logOn=true";
            $("#loginLink").attr("href", href).click();
            $("#username").text(nickname);                      //delete
        }
    });

    $("#textusername").keydown(function (e) {
        if(e.keyCode == 13) {
            e.preventDefault();
            $("#btnlogin").click();
        }
    });
});

function LoginOnSuccess(result)
{
    Scroll();
    setTimeout("Refresh();", 5000);

    $("#txtmessage").keydown(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $("#btnmessage").click();
        }
    });

    $("#btnmessage").click(function () {
        var text = $("#txtmessage").val();
        if (text) {
            var href = "/Home?user=" + encodeURIComponent($("#username").text());
            href = href + "&chatmessage=" + text;
            $("#txtmessage").val("");
            $("#updatelink").attr("href", href).click();
        }
    });

    $("#btnlogoff").click(function () {
        var href = "/Home?user=" + encodeURIComponent($("#username").text());
        href = href + "&logOff=true";
        $("#updatelink").attr("href", href).click();
        document.location.href = "Home";
    });
}

function LoginOnFailure(result) {
    $("#username").val("");
    $("#error").text(result.responseText);
    setTimeout("$('#error').empty();", 2000);
}

function Refresh(){
    var href = "/Home?user=" + encodeURIComponent($("#username").text());
    $("#updatelink").attr("href", href).click();
    setTimeout("Refresh();", 5000);
}

function ChatOnFailure(result) {
    $("#error").text(resutl.responseText);
    setTimeout("$('#error').empty();", 2000);
}

function ChatOnSuccess(result) {
    Scroll();
}

function Scroll() {
    var win = $("#messages");
    var height = win[0].scrollHeight;
    //alert(height);
    //alert(window.pageYOffset);
    win.scrollTop(height);
}
