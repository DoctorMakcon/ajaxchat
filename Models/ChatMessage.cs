﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AjaxChat.Models
{
    public class ChatMessage
    {
        public ChatUser user;
        public DateTime messageTime;
        public string text = "";
    }
}