﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AjaxChat.Models
{
    public class Chat
    {
        public List<ChatMessage> messages;
        public List<ChatUser> users;

        public Chat()
        {
            users = new List<ChatUser>();
            messages = new List<ChatMessage>();

            messages.Add(new ChatMessage { text = "Chat started!", messageTime = DateTime.Now});
        }
    }
}