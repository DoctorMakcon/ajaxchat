﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AjaxChat.Models;
using System.Drawing;

namespace AjaxChat.Controllers
{
    public class HomeController : Controller
    {
        static Chat chat;

        private Color RandomColor()
        {
            Random rnd = new Random();
            return Color.FromArgb(rnd.Next(255), rnd.Next(255), rnd.Next(255));
        }
        public ActionResult Index(string user, bool? logOn, bool? logOff, string chatMessage)
        {
            try
            {
                if (chat == null)
                    chat = new Chat();

                if (chat.messages.Count > 100)
                    chat.messages.RemoveRange(90, 10);

                if (!Request.IsAjaxRequest())
                    return View(chat);
                else
                {
                    if (logOn != null && (bool)logOn)               //if new user
                    {
                        if (chat.users.FirstOrDefault(c => c.name == user) != null)              //current user exist in chat
                            throw new Exception("Entered login is used in chat");
                        else
                        {
                            chat.users.Add(new ChatUser { name = user, loginTime = DateTime.Now, color = RandomColor() });
                            chat.messages.Add(new ChatMessage { text = user + " entered in chat!", messageTime = DateTime.Now });
                        }
                        return PartialView("ChatRoom", chat);
                    }
                    else
                    {
                        if (logOff != null && (bool)logOff)       //if user left
                        {
                            chat.users.Remove(chat.users.FirstOrDefault(c => c.name == user));
                            chat.messages.Add(new ChatMessage { text = user + " left chat!", messageTime = DateTime.Now });
                            return PartialView("ChatRoom", chat);
                        }
                        else                                     //if new message
                        {
                            if (!string.IsNullOrEmpty(chatMessage))
                            {
                                ChatUser u = chat.users.FirstOrDefault(c => c.name == user);
                                chat.messages.Add(new ChatMessage { text = chatMessage, messageTime = DateTime.Now, user = u });
                            }
                            return PartialView("History", chat);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                return Content(ex.Message);
            }
        }
    }
}
